import React from "react";

// Creating the Context Object
// A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the React app.
const UserContext = React.createContext();

// The "Provider" component allows other components to consume/use the context object and supply the necessary information needed to the context object
// Any component which is not wrapped by our Provider will not have access to the value provided in the context.
export const UserProvider = UserContext.Provider;

export default UserContext;