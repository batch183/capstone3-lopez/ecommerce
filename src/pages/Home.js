import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "Mansanas",
		content: "Come get your latest gadgets from Mansanas",
		destination: "/products",
		label: "Buy Now"
	}

	return(
		<>
			<Banner data={data}/>
        	<Highlights />
		</>
	)
}