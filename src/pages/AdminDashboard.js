import { useContext, useState, useEffect } from "react";
import {Table, Button, Modal,Badge} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import NewProduct from "../components/NewProduct";
//import EditProduct from "../components/EditProduct";

import Swal from "sweetalert2";

export default function AdminDashboard(){

	// to validate the user role.
	const {user} = useContext(UserContext);
	const [show, setShow]=useState(false);
	const handleShow = () => setShow(true);
	const handleClose = () => setShow(false);
	//const [show2, setShow2]=useState(false);
	//const handleShow2 = () => setShow(true);
	//const handleClose2 = () => setShow2(false);


	
	const [allProducts, setAllProducts] = useState([]);

	
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stocks}</td>
						<td>{product.isActive ? <><Badge pill bg="success">Active</Badge></> : <><Badge pill bg="secondary">Inactive</Badge></>}</td>
						<td>
							{
								(product.isActive)
								?
									<div className="text-center"><Button variant="danger" size="sm" onClick ={() => archive(product._id, product.name)}>Archive</Button></div>
								:
									<><div className="text-center">
										<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" as={Link} to={`/products/${product._id}/edit`}>Edit</Button>
										</div>
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}



	
	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	
	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	/*const editProduct = (productId) =>{
		console.log(productId);
		setShow2(true);
		return productId;
	}*/

	const addProduct = () => {
		handleClose();
		fetchData();
	}

	useEffect(()=>{
		fetchData();
	})

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<Button variant="primary" size="lg" className="mx-2" onClick={handleShow}>Add Product</Button>
				<Button variant="success" size="lg" className="mx-2" >Show Orders</Button>
			</div>
			<Table striped bordered hover className="mx-2">
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Stocks</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>

		   <Modal show={show} onHide={handleClose}>
		   	<Modal.Header>
		   	 <Modal.Title>
		   	 	Add Product
		   	 </Modal.Title>	
		   	</Modal.Header>
		   	<Modal.Body>
		   		<NewProduct addModal={addProduct} />
		   	</Modal.Body>
		   	<Modal.Footer>
		   		<Button variant="secondary" onClick={handleClose}>
		   			Close
		   		</Button>
		   	</Modal.Footer>
		   	</Modal>

		   	
		</>
		:
		<Navigate to="/products" />
	)
}