import { useState, useEffect, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap";

import UserContext from "../UserContext";

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);


	const order = (productId,productQuantity) => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/new`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				productQuantity: productQuantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Succesfully ordered!",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})
				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	useEffect(()=>{
		if(quantity!==""){
			setTotalAmount(quantity*price);
		}
		else{
			setTotalAmount(0);
		}
	},[totalAmount,price,quantity])



	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center text-dark">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Card.Subtitle>Total:</Card.Subtitle>
							<Card.Text>PhP {totalAmount}</Card.Text>
							<Form>
								<Form.Group className="mb-3" controlId="mobileNo">
									<Form.Label>Quanity</Form.Label>
									<Form.Control type="number" min="0" max="" placeholder="Quanity" value={quantity} onChange={e => setQuantity(e.target.value)}/>
								</Form.Group>
							</Form>
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
								<Button variant="primary" size="lg" onClick={() => order(productId,quantity)}>Order
								</Button>
								:
								<Button as={Link} to="/login" variant="primary" size="lg">Login to Order</Button>
							}
							</div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
	)
}