//Applying bootstrap grid system
import {Row, Col, Card} from "react-bootstrap";

export default function Highlights(){
	return(
		<Row className="mt-3 mb-3 p-3">
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Guaranteed Brand New</h2>
		                </Card.Title>
		                <Card.Text>
		                    Our store has everything from smartphones, to laptops, to smart TVs and more. We only sell brand new gadgets at below retail prices.
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>2 Year Warranty</h2>
		                </Card.Title>
		                <Card.Text>
		                    Did you know that the average cost of repairing a product is $84? Every purchase comes with a 2-year warranty and you can get your broken product fixed for free!
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		    <Col xs={12} md={4}>
		        <Card className="cardHighlight p-3">
		            <Card.Body>
		                <Card.Title>
		                    <h2>Always Up to date</h2>
		                </Card.Title>
		                <Card.Text>
		                    People are always looking for the newest and most innovative technology. That's why we've made it our mission to bring the newest and best products to your doorstep. We have the latest smartphones, laptops, smart watches, drones, and more!
		                </Card.Text>
		            </Card.Body>
		        </Card>
		    </Col>
		</Row>
	)
}