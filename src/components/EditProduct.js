import {useState, useEffect/*, useContext*/} from "react";
//import UserContext from "../UserContext";
import {/*Navigate,*/ useNavigate,useParams} from "react-router-dom";

import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";

export default function EditProduct({productProp}){


	//const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const { productId } = useParams();
	
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stocks, setStock] = useState("");

	console.log(productName);
	console.log(description);
	console.log(price);
	console.log(stocks);

	function editProducts(e,productId){
		e.preventDefault();	

		
			
				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/edit`, {
					method: "PUT",
					headers:{
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: productName,
						description: description,
						price: price,
						stocks: stocks
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						setProductName("");
						setDescription("");
						setPrice("");
						setStock("");

						Swal.fire({
							title: "Product updated!",
							icon: "success",
							text: "Start Selling"
						})

						navigate("/admin");
						

					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})

	}

	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(productName !== "" && description !== "" && price !== "" && stocks !== "" ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[productName, description, price, stocks])

	useEffect(() =>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setStock(data.stocks);
		})

	}, [productId])

	return(
		<>
		<div className="d-grid gap-3 pb-3 px-3 pt-3">
		<h1>Edit Product</h1>
		<Form onSubmit = {(e) => editProducts(e,productId)}>
			<Form.Group className="mb-3">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control type="text" placeholder="Product Name" value={productName} onChange={e => setProductName(e.target.value)} />
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Description</Form.Label>
				  <Form.Control type="textarea" placeholder="Description" value={description} onChange={e => setDescription(e.target.value)} />
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Price</Form.Label>
				  <Form.Control type="number" placeholder="Price" value={price} onChange={e => setPrice(e.target.value)}/>
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Stocks</Form.Label>
				  <Form.Control type="number" placeholder="Stocks" value={stocks} onChange={e => setStock(e.target.value)}/>
			</Form.Group>
			
			
			{
		      	isActive
		      	?
		      		<Button variant="success" type="submit" id="submitBtn">
		      		  Edit Product
		      		</Button>
		      	:
		      		<Button variant="success" type="submit" id="submitBtn" disabled>
		      		  Edit Product
		      		</Button>
		    }
		</Form>
		</div>
		</>
		

	)
}