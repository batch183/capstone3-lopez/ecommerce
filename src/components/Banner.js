import {Row, Col, Button} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	console.log(data);

	const {title, content, destination, label, image} = data;

	return(


		<Row>
			<Col className="p-5 text-center">

				<h1>{title}</h1>
				<p>{content}</p>
				<img className="w-100 pb-3" src={image} alt=""/>
				<Button className="p-2"as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
}