import {useState, useEffect/*, useContext*/} from "react";
//import UserContext from "../UserContext";
import {/*Navigate,*/ useNavigate} from "react-router-dom";

import Swal from "sweetalert2";
import { Form, Button } from "react-bootstrap";

export default function NewProduct({addModal}){

	//const {user} = useContext(UserContext);
	const navigate = useNavigate();
	const [show, setShow]=useState(false);
	//const handleShow = () => setShow(true);
	const handleClose = () => setShow(false);

	//State hooks to store the values of the input fields
	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [stocks, setStock] = useState("");

	//Check if the values are successfully binded/passed.
	console.log(productName);
	console.log(description);
	console.log(price);
	console.log(stocks);
	console.log(show);

	// Function to simulate user registration
	function newProduct(e){
		//prevents the page redirection via form submit
		e.preventDefault();

		//Checking if the email is still available
		
			
				fetch(`${process.env.REACT_APP_API_URL}/products/new`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem("token")}`
					},
					body: JSON.stringify({
						name: productName,
						description: description,
						price: price,
						stocks: stocks
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setProductName("");
						setDescription("");
						setPrice("");
						setStock("");

						Swal.fire({
							title: "Product added!",
							icon: "success",
							text: "Start Selling"
						})

						//redirect the user to the login page after registration.
						navigate("/admin");
						addModal();

					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
		
		


		// alert("Thank you for registering!");
		// email = "";
		// password1 = "";
		// password2 = "";

	}

	const [isActive, setIsActive] = useState(false);

	useEffect(()=>{
		if(productName !== "" && description !== "" && price !== "" && stocks !== "" ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[productName, description, price, stocks])

	return(

		<Form onSubmit = {(e) => newProduct(e)}>
			<Form.Group className="mb-3">
				  <Form.Label>Product Name</Form.Label>
				  <Form.Control type="text" placeholder="Product Name" value={productName} onChange={e => setProductName(e.target.value)} />
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Description</Form.Label>
				  <Form.Control type="textarea" placeholder="Description" value={description} onChange={e => setDescription(e.target.value)} />
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Price</Form.Label>
				  <Form.Control type="number" placeholder="Price" value={price} onChange={e => setPrice(e.target.value)}/>
			</Form.Group>
			<Form.Group className="mb-3">
				  <Form.Label>Stocks</Form.Label>
				  <Form.Control type="number" placeholder="Stocks" value={stocks} onChange={e => setStock(e.target.value)}/>
			</Form.Group>
			
			
			{
		      	isActive
		      	?
		      		<Button variant="success" type="submit" id="submitBtn" onClick={handleClose}>
		      		  Add Product
		      		</Button>
		      	:
		      		<Button variant="success" type="submit" id="submitBtn" disabled>
		      		  Add Product
		      		</Button>
		      }
		</Form>

	)
}